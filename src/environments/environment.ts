// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { MockCategorieService } from "src/app/service/mock/categorie.service";



export const environment = {
  production: false,
  titre: "Test",
  providers: {
    MENU: MockCategorieService
  },
  notification: {
    duration: 2000
  },
  facture: {
    coutlivraison: '250',
  },

  adress: {
    adress: '00251548',
  },
  values: {
    CORDOVA_PLATEFORM: 'cordova',
    USER_PHONE: 'userphone',
    USER_NAME: 'username',
    USER_FIRSTNAME: 'userfirstname',
    USER_ADDRESS: 'useraddress',
    USER_HISTORY: 'userhistory',
    SDR_MENU: 'listMenu',
    APP_NAME: 'SDR',
    USER_CART: 'panie'
  },
  values_history: {
    CORDOVA_PLATEFORM: 'cordova',
    HISTORY_NUMBER: 'historynumber',
    HISTORY_ORDER: 'historyorder',
    HISTORY_PRICE: '0',
    HISTORY_DELEVERY: 'historydelevery',
    HISTORY_PRODUCT: 'historyproduct'

  },
  api: {
    basePath: 'https://sdr.api.rintio.com/', // "http://192.168.43.101/sdr_api/api/",
    menuEndpoint: 'sf/index.php?rquest=getmenu',
    commandeEndpoint: 'sf/index.php?rquest=commander',
    errorEndpoint: 'sf/index.php?rquest=error',
  },

  message: {
    MENU_LOADING: 'Chargement du menu',
    erreur: 'Service indisponible',
    SERVICE_UNAVAILABLE: 'Impossible de joindre le serveur Vérifier votre connexion',
    NO_DATA: "Aucune description"
  },

  image: {
    url: 'assets/imgs/burgerIcon.gif',
    description: 'Aucune description',
  },

  livraison: {
    message:
      '*Notre service de livraison est disponible de 8h à 18h. Merci pour votre fidélité!',
    lieulivraison:
      'Service de livraison disponible sur Cotonou, Godomey et Calavi. ',
    heureouverture: '*Nous sommes ouverts de 8h à 22h ',
  },
  OneSignal: {
    signal_app_id: '3895a983-fe9d-4d23-9464-76b630c49d13',
    firebase_id: '429880702837',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */

