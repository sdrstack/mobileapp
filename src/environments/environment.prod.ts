import { CategorieService } from "src/app/service/api/categorie.service";


export const environment = {
  production: true,
  titre : "production",
  providers: {
    MENU: CategorieService
  },
  api: {
    basePath: 'https://sdr.api.rintio.com/',
    menuEndpoint: 'sf/index.php?rquest=getmenu',
    commandeEndpoint: 'sf/index.php?rquest=commander',
    errorEndpoint: 'sf/index.php?rquest=error',
  }
};
