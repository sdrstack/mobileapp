
export interface User {
  name: string,
  firstname: string,
  telephone: string,
  address: string,
}


export interface ProductComposition {

  imageUrl: string;
  name: string;
}

export interface Product {
  id : number;
  image: string;
  titre: string;
  livrable: boolean;
  prix: number;
  description: string;
  prix_horsTaxe: number;
  taxe: number;
  ingredients: ProductComposition[];
  restaurant : string;
}

export interface Categorie {
  cat: string;
  repas: Product[];
}

export interface Place {
  name: string;
  locations: string[];
  price: string;
}

export interface OrderItem {
  item: Product;
  quantity: number;
  amount: number;
}

export interface Order {
  number: string;
  delivery: boolean;
  deliveryFees: number;
  totalAmount: number;
  dateDelivery: string;
  note: string;
  item: OrderItem[];
}

export interface SliderItem{
  title: string,
  description: string,
  image: string,
  show:boolean
}

