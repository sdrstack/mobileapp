import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'appfilter' })
export class FilterPipe implements PipeTransform {
  /**
   * Pipe filters the list of elements based on the search text provided
   *
   * @param list list of elements to search in
   * @param searchText search string
   * @returns list of elements filtered by search text or []
   */
  transform(list: any[], searchText: string, fieldName: string): any[] {
    if (!searchText) { return list; }
    return list.filter(item =>item[fieldName].toLowerCase().includes(searchText.toLowerCase()));
  }
}

@Pipe({ name: 'xofCurrency' })
export class XofPipe implements PipeTransform {

  transform(amount: string): string {
    return (amount == undefined || amount == null)? '': amount.toLocaleString()
  }
}
@Pipe({ name: 'twoDigit' })
export class TwoDigitPipe implements PipeTransform {

  transform(digit: number): string {
    return (digit < 10) ?  '0'+digit.toString() : digit.toString()
  }
}
@Pipe({ name: 'timeAgo' })
export class TimeAgo implements PipeTransform {
  transform(d: any): string {

    let currentDate = new Date(new Date().toUTCString());
    let date = new Date(d);

    let year = currentDate.getFullYear() - date.getFullYear();
    let month = currentDate.getMonth() - date.getMonth();
    let day = currentDate.getDate() - date.getDate();
    let hour = currentDate.getHours() - date.getHours();
    let minute = currentDate.getMinutes() - date.getMinutes();
    let second = currentDate.getSeconds() - date.getSeconds();
    d =new Date(d);
    let months = ["Jan", "Fév", "Mars", "Avr", "Mai", "Juin", "Juil", "Août", "Spet", "Oct", "Nov", "Déc"];
    let THEmonth=d.getMonth();
    d = d.getDate()+" "+(months[THEmonth])+", "+d.getFullYear()+"  à "+d.getHours()+"h"+d.getMinutes();

    let createdSecond = (year * 31556926) + (month * 2629746) + (day * 86400) + (hour * 3600) + (minute * 60) + second;
    if (createdSecond >= 31556926) {
      return d;
    } else if (createdSecond >= 2629746) {
      return d;
    } else  if (createdSecond >= 86400) {
      let dayAgo = Math.floor(createdSecond / 86400);
        if(dayAgo == 1){
          return "Hier";
        }else if(dayAgo == 2){
          return "Avant hier"
        }else if(dayAgo > 2){
          return d;
      }
    }else if (createdSecond >= 3600) {
      let hourAgo = Math.floor(createdSecond / 3600);
      return hourAgo > 1 ? hourAgo + " heurs" : hourAgo + " heur";
    } else if (createdSecond >= 60) {
      let minuteAgo = Math.floor(createdSecond / 60);
      return minuteAgo > 1 ? minuteAgo + " minutes" : minuteAgo + " minute";
    } else if (createdSecond < 60) {
      //console.log(date)
      return createdSecond > 30 ? createdSecond + " s" : " Maintenant";
    }
    //
  }
}



@Pipe({ name: 'catfilter', pure : false })
export class FilterCatPipe implements PipeTransform {
  /**
   * Pipe filters the list of elements based on the search text provided
   * @param listMenu list of elements to search in
   * @param searchs search  array
   * @returns list of elements filtered by search text or []
   */

  transform(listMenu: any[], searchs: string [],): any[] {
    return (searchs.length==0) ? listMenu:listMenu.filter(({ cat }) => searchs.includes(cat.toLowerCase()));
  }
}
