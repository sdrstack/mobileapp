import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

/**
* Api is a generic REST Api handler. Set your API basePath first.
*/
@Injectable()
export class Api {
  protected basePath = '';

  constructor(public http: HttpClient, public server: string, public port: string = undefined) {
    this.basePath = server;
    if (port !== undefined) { this.basePath += ':' + port; }
  }

  get(endpoint: string, params?: any, reqOpts?: any): Observable<any> {
    if (!reqOpts) {
      reqOpts = {
        params: new HttpParams()
      };
    }

    // Support easy query params for GET requests
    if (params) {
      reqOpts.params = new HttpParams();
      for (let k in params) {
        reqOpts.params = reqOpts.params.set(k, params[k]);
      }
    }

    return this.http.get(this.basePath + '/' + endpoint);

  }

  post(endpoint: string, body: any, reqOpts?: any) {

    return this.http.post(this.basePath + '/' + endpoint, body, reqOpts);

  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.basePath + '/' + endpoint, body, reqOpts);

  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http.delete(this.basePath + '/' + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return this.http.patch(this.basePath + '/' + endpoint, body, reqOpts);

  }
}