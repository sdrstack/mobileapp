import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SharedDataService {

    private userName = new Subject<any>();

    updateUserName(userName: any) {
        this.userName.next(userName);
    }

    getUserName(): Subject<any> {
        return this.userName;
    }
}