import { Injectable } from '@angular/core';
import { Subscription, BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DatapasseService {

  constructor() { }

  subscription: Subscription;

  // envoyer la list du menu vers la page filter
  private listMenu: any = new BehaviorSubject<string>(null);
  currentListMenu: any = this.listMenu.asObservable();

  passList(query: any) {
    this.listMenu.next(query);
  }

  // envoyer la catégorie selectionné dans la page filter
  private catSource: any = new BehaviorSubject<string>(null);
  currentCatSource: any = this.catSource.asObservable();

  passCat(query: any) {
    this.catSource.next(query);
  }

  // envoyer la liste des elements du panier 
  private panier: any = new BehaviorSubject<string>(null);
  currentPanier: any = this.panier.asObservable();

  passPanier(query: any) {
    this.panier.next(query);
  }

  // envoyer la liste des elements du panier 
  private commande: any = new BehaviorSubject<string>(null);
  currentCommande: any = this.commande.asObservable();

  passCommande(query: any) {
    this.commande.next(query);
  }

  // envoyer le numéro de la commande
  private created: any = new BehaviorSubject<string>(null);
  currentCreated: any = this.created.asObservable();

  passCreated(query: any) {
    this.created.next(query);

  }

  // envoyer la description au modal description
  private descrip: any = new BehaviorSubject<string>(null);
  currentDescrip: any = this.descrip.asObservable();

  passDescrip(query: any) {
    this.descrip.next(query);
  }


  // envoyer les ingredient au modal ingredient
  private ingredient: any = new BehaviorSubject<string>(null);
  currentIngredient: any = this.ingredient.asObservable();

  passIngredient(query: any) {
    this.ingredient.next(query);
  }

  // envoyer les ingredient choisie au userhome
  private selOptions: any = new BehaviorSubject<string>(null);
  currentChoiceIngredient: any = this.selOptions.asObservable();

  passChoiceIngredient(query: any) {
    this.selOptions.next(query);
  }
}
