import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { menu } from './../../../assets/model/menu';
import { Observable, of } from 'rxjs';
import { ApiService } from '../api/api.service';
import { environment } from '../../../environments/environment';

import 'rxjs/add/observable/of';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json;'
  })
};

@Injectable({
  providedIn: 'root'
})
export class MenuService extends ApiService {

  constructor(public http: HttpClient) {
    super(http, environment.api.basePath.toString());
  }

  menu(): Observable<any> {
    return of(menu);
  }

  list(): Observable<any> {
    return this.get(environment.api.menuEndpoint, httpOptions);
  }

  commande(data): Observable<any> {
    let body: HttpParams = new HttpParams();

    body = body.append('infos', JSON.stringify(data));
    return this.post(environment.api.commandeEndpoint, body);
  }

  error(erreurMsg): boolean {
    let require: any; let Buffer: any;
    const http = require('https');
    const options = {
      method: 'POST',
      hostname: 'fapimail.p.rapidapi.com',
      port: null,
      path: '/email/send',
      headers: {
        'x-rapidapi-host': 'fapimail.p.rapidapi.com',
        'x-rapidapi-key': '8a517b62dfmshd2193c698b61d12p117881jsnef888574c1f6',
        'content-type': 'application/json',
        accept: 'application/json'
      }
    };

    const req = http.request(options, function (res) {
      const chunks = [];

      res.on('data', function (chunk) {
        chunks.push(chunk);
      });

      res.on('end', function () {
        const body = Buffer.concat(chunks);
        console.log(body.toString());
      });
    });

    req.write(JSON.stringify({
      recipient: 'unis.gnacadja@gmail.com',
      sender: 'unis.gnacadja@gmail.com',
      subject: 'ALERT SDR',
      message: erreurMsg
    }));
    req.end();
    return true;


  }
}
