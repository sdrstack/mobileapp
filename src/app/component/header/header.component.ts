import { Component, ChangeDetectionStrategy, Input,  } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { OrdersService } from '../../service/mock/orders.service'
import { UserService } from '../../service/mock/user.service'

export const HOME_PAGE = 'home';
export const INTRO_PAGE = "intro";
export const LOGIN_PAGE = "login";
export const PROFIL_PAGE = "profil";
export const HISTORY_PAGE = "history";
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent{

  userName: string;
  showname: boolean = false;
  @Input() context = HOME_PAGE;
  nbProduct: Observable<number>;
  subscription: Subscription;
  UserInfos: Observable<any> | undefined;


  constructor(
    private router : Router,
    private Order: OrdersService,
    private UserService : UserService
  ) {
  }

  ngOnInit(): void {
    this.UserInfos = this.UserService.userProfil$;
    this.nbProduct = this.Order.currentInfosProduct$;
    this.UserService.userProfil$.subscribe(item => this.userName = (item.firstname > 8) ? item.firstname.substring(0,1)+'.' : item.firstname);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  openCard() {

    this.router.navigateByUrl('/card');
  }
}


