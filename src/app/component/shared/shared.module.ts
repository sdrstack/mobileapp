import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from '../header/header.component';
import { HistoryItemComponent } from '../history-item/history-item.component';
import { FilterSlidesComponent } from 'src/app/component/filter-slides/filter-slides.component';


@NgModule({
  declarations: [HeaderComponent, HistoryItemComponent,FilterSlidesComponent],
  imports: [
    CommonModule,

  ],
  exports: [HeaderComponent,FilterSlidesComponent]
})
export class SharedModule { }
