import { Component, OnInit,Input, Output, EventEmitter  } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SliderItem } from 'src/app/models/models';


@Component({
  selector: 'app-filter-slides',
  templateUrl: './filter-slides.component.html',
  styleUrls: ['./filter-slides.component.scss'],
})
export class FilterSlidesComponent implements OnInit {
  @Input() item : SliderItem = {
    show : true,
    title : '',
    description: '',
    image: null,
  };
  @Output() sendItem = new EventEmitter<string>();


 private  categorieTitle: BehaviorSubject<string> = new BehaviorSubject<string>(this.item.title);
 public readonly categoryTitle$ : Observable<string> =  this.categorieTitle.asObservable();

 ngOnInit(){
   
 }
 /*
  *
  *@params : item.title (string)
  *@outpout : title (string)
  * Resume : Permet l'envoie du title de la catégorie choisit
  * au composant Parent afin d'effectuer le filtre sur le menus affiché.
  */
  setItem() {
    this.item.show = !this.item.show; // fill or unfill button

    this.categorieTitle.next(this.item.title);
    this.sendItem.emit(this.item.title);
  }
}
