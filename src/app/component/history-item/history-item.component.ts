import { Component, Input, OnInit } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { OrdersService } from 'src/app/service/mock/orders.service';
import { ToastController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
registerLocaleData(localeFr, 'fr');
@Component({
  selector: 'app-history-item',
  templateUrl: './history-item.component.html',
  styleUrls: ['./history-item.component.scss'],
})
export class HistoryItemComponent implements OnInit {
  nb:number=1;
  src : string;
  items:any;

  @Input() item = <any>{
    "number": 7,
    "date": "2014-10-03 06:46:06 ",
    "price": "2500",
    "delevery": false,
    "product": [
      {
        "id": 8,
        "name": "Trevino",
        "quantity": 50
      },
      {
        "id": 7,
        "name": "Terry",
        "quantity": 18
      },
      {
        "id": 1,
        "name": "Blake",
        "quantity": 85
      }
    ]
  };


  constructor(public Order: OrdersService,
    public toastCtrl: ToastController,) {

  }

  ngOnInit() {
    this.src="../../../assets/imgs/arrow-drop-down.svg"
    
  }

  play() {
    
    this.nb =(this.nb)? 0:1;
    return this.src= (this.nb)? "../../../assets/imgs/arrow-drop-down.svg":"../../../assets/imgs/arrow-drop-up.svg";
    

  }

  getState() {
    
    return ((this.item.deliveryFees) ? 'Livré' : 'Sur place')
    }
    

  addToCart() {
    
    this.item.item.forEach((product) => {

      this.Order.saveToCard(product.item);
      

    })

    this.presentToast(this.item.item.length.toString());


  }

  async presentToast(msg: string) {
    const toast = await this.toastCtrl.create({
      message: msg + ' produit ajouté(s)',
      duration: environment.notification.duration
    });
    toast.present();
  }


}
