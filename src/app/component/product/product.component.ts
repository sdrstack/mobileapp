import { environment } from '../../../environments/environment';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { OrdersService } from '../../service/mock/orders.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})

export class ProductComponent implements OnInit {

  @Input() product: any = {
    image: environment.image.url,
    restaurant: "Sandwich du Roi",
    livrable: true,
    titre: "Pizza au poulet",
    description: environment.message.NO_DATA,
    ingredients: [],
    prix: 2000,
  };

  selected: boolean = false;
  constructor(public router: Router,
    public toastCtrl: ToastController,
    public Order: OrdersService
  ) { }

  ngOnInit(): void {
    if (this.product.description === false) this.product.desciption = environment.message.NO_DATA;
  }

  viewMore() {
    this.router.navigateByUrl("/product");
  }

  async presentToast() {
    const toast = await this.toastCtrl.create({
      message: this.product.titre + ' a été ajouté au panier.',
      duration: environment.notification.duration
    });
    toast.present();
  }

  LoadingFailed() { this.product.image = environment.image.url; }

  async showIngredient() { }

  addToCart() {
    this.selected = true;
    this.presentToast();
    this.Order.saveToCard(this.product);
    
  
  }

}
