import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-popover-orderconfirm',
  templateUrl: './popover-orderconfirm.component.html',
  styleUrls: ['./popover-orderconfirm.component.scss'],
})
export class PopoverOrderconfirmComponent implements OnInit {

  @Input() message: string;

  constructor(
    private router: Router,
    private navCtrl:NavController,
    public popoverController: PopoverController,
  ) { }

  ngOnInit() {}

  quit(){
    this.router.navigateByUrl('/history');
    this.popoverController.dismiss();
  }

}
