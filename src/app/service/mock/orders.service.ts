import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';
import { Order } from '../../models/models';
import { Plugins } from '@capacitor/core';
import { environment } from '../../../environments/environment';
import { IOrdersService } from '../interface/order.interface';

@Injectable({
  providedIn: 'root'
})
export class OrdersService implements IOrdersService {

  orders: any[] = [];
  the_orders_old:Order;
  currentOrder: Order = {
    number: '',
    delivery: false,
    dateDelivery: new Date(Date.now() + 20 * 60 * 1000).toISOString(),  // set 20minutes delay
    deliveryFees: 0,
    totalAmount: 5000,
    note: '',
    item: []
  };


  // setting an Observable to nbProduct in card
  private infosProduct: BehaviorSubject<number> = new BehaviorSubject(this.currentOrder.item.length);
  public readonly currentInfosProduct$: Observable<number> = this.infosProduct.asObservable();
  constructor(
  ) {

    try {
      (localStorage.getItem(environment.values.USER_HISTORY)) ? this.orders = JSON.parse(localStorage.getItem(environment.values.USER_HISTORY)) : this.orders = [];
    } catch (e) {
      this.orders = [];
    }



  }



  getOrders(): Observable<any[]> {
    return of(this.orders);

  }

  getCard() {
    return of(this.currentOrder.item);
  }
  get(id: number): Observable<any> {
    return of(this.orders[id]);

  }

  saveToCard(P: any) {
    let seachIndex = this.currentOrder.item.findIndex(function (object) {
      return object.item.id === P.id;
    });


    (seachIndex == -1) ? this.currentOrder.item.push({ item: P, quantity: 1, amount: P.prix })
      : this.currentOrder.item[seachIndex].quantity += 1;
    this.infosProduct.next(this.currentOrder.item.length); // Notify others components on Current Product size change
  }

  isdeliverable(): boolean {
    let delivery = true;
    this.currentOrder.item.forEach(item => {
      delivery = delivery && item.item.livrable;
    })

    return delivery;
  }

  addQuantity(P: any, i: number) {
    P.item[i].quantity += 1;
  }
  removeQantity(P: any, i: number) {
    P.item[i].quantity = (P.item[i].quantity == 1) ? 1 : P.item[i].quantity - 1;
    ;
  }

  sendOrder(order: any) {
    order.number = 12345678;
    this.orders.push(order);

    return of(order);
  }

  getcurrentOrder() {

    return of(this.currentOrder);
  }

  removeOrderfromcard(i){
    this.currentOrder.item.splice(i, 1);
    this.infosProduct.next(this.currentOrder.item.length); // Notify others components on Current Product size change

  }

  removeAllOrderfromcard(){
    this.currentOrder.item.splice(0, this.currentOrder.item.length);
    this.infosProduct.next(this.currentOrder.item.length); // Notify others components on Current Product size change

  }

  removecurrentOrder() {

    this.currentOrder = {
      number: '',
      delivery: false,
      dateDelivery: new Date(Date.now() + 20 * 60 * 1000).toISOString(),  // set 20minutes delay
      deliveryFees: 0,
      totalAmount: 0,
      note: '',
      item: []
    };

    this.infosProduct.next(this.currentOrder.item.length); // Notify others components on Current Product size change


    this.getcurrentOrder();
    return this.currentOrder;

  };

  saveOrder(){
    localStorage.setItem(environment.values.USER_HISTORY, JSON.stringify(this.orders));

  }


}
