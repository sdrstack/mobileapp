import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';
import { Place } from '../../models/models';

import { orders as data } from '../data/places';
import { IPlaceService } from '../interface/place.interface';


@Injectable({
  providedIn: 'root'
})
export class PlaceService implements IPlaceService {
  places: Place[];

  constructor() {

    try {
      this.places = <any[]>data;
    } catch (error) {

    }



  }

  getAll(): Observable<Place[]> {
    return of(this.places);
  }

  get() {

    return this.places[0];
  }
}
