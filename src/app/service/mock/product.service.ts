import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';
import { Product } from '../../../app/models/models';
import { IProductService } from '../interface/Product.interface';
import { menu as data } from '../data/menu';


@Injectable({
  providedIn: 'root'
})
export class ProductService implements IProductService {
  products: Product[];

  constructor() {
  }

  getAll(): Observable<Product[]> {
    return of(this.products);
  }

  get() {

    return this.products[0];
  }
}
