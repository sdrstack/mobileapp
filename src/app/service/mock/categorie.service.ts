import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';
import { Categorie } from '../../models/models';

import { menu as data } from '../data/menu';
import { ICategorieService } from '../interface/categorie.interface';


@Injectable(
  {
  providedIn: 'root'
  }

)
export class MockCategorieService implements ICategorieService {
  categories: Categorie[];

  constructor() {

    try {this.categories = <any[]>data;} catch (error) {

    }



  }

  getAll(): Observable<Categorie[]> {
    return of(this.categories);
  }
}
