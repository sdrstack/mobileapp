import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'src/app/models/models';
import { IUserService } from '../interface/user.interface';

const { Storage } = Plugins;


@Injectable({
  providedIn: 'root'
})
export class UserService implements IUserService {

  private user: User = {
    name: '',
    firstname: '',
    telephone: '',
    address: '',
  }

  private userProfil: BehaviorSubject<any> = new BehaviorSubject(this.user);
  public readonly userProfil$: Observable<any> = this.userProfil.asObservable();

  constructor() {
    try {
      this.user = JSON.parse(localStorage.getItem('USER_IDENTIY'));
    }
    catch (e) { console.log(e) }
    this.userProfil.next(this.user);

  }

  getUser() {
    return this.userProfil$;
  }


  Update(data: User) {
    this.user = data;
    Storage.set({
      key: 'USER_IDENTITY',
      value: JSON.stringify(this.user)
    });

    this.userProfil.next(this.user);
  }
}
