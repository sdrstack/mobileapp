import { Observable } from "rxjs/internal/Observable";
import { Categorie } from "src/app/models/models";

export interface ICategorieService {

  getAll(): Observable<Categorie[]>;
}
