import { Observable } from "rxjs/internal/Observable";
import { Place } from "src/app/models/models";

export interface IPlaceService {

  getAll(): Observable<Place[]>;
}
