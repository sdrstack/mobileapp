import { Observable } from "rxjs/internal/Observable";
import { Product } from "src/app/models/models";

export interface IProductService {

  getAll(): Observable<Product[]>;
}
