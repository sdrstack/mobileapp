import { Observable } from "rxjs/internal/Observable";
import { Order } from "src/app/models/models";

export interface IOrdersService {


  getOrders(): Observable<Order[]>;
  saveToCard(P: any);
  isdeliverable(): boolean;
  addQuantity(P: any, i: number);
  removeQantity(P: any, i: number);
  sendOrder(order: any);
  getcurrentOrder(): Observable<Order>;
  removeOrderfromcard(i: number);
  removecurrentOrder();
  saveOrder();

}
