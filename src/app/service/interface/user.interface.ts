import { User } from "src/app/models/models";

export interface IUserService {

  getUser();
  Update(data: User);
}
