export const orders = <any>[
  {
    name: 'Zone1',
    locations: ['agla','ste rita', 'vedoko'],
    price: 100
  },
  {
    name: 'Zone2',
    locations: ['loc1', 'loc2', 'loc3'],
    price: 200
  },
  {
    name: 'Zone3',
    locations: ['quartier1', 'quartier2', 'quartier3'],
    price: 300
  }
]
