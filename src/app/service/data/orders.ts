export const orders = <any>[
  {
    "_id": 3,
    "date": "2021-10-22 11:20:30 ",
    "price": "30877",
    "delevery": false,
    "product": [
      {
        "id": 6,
        "name": "Wright",
        "quantity": 49
      }
    ]
  },
  {
    "_id": 8,
    "date": "2021-10-21 10:35:53 ",
    "price": "1642",
    "delevery": true,
    "product": [
      {
        "id": 7,
        "name": "Nunez",
        "quantity": 95
      },
      {
        "id": 9,
        "name": "Downs",
        "quantity": 50
      },
      {
        "id": 2,
        "name": "Durham",
        "quantity": 28
      }
    ]
  },
  {
    "_id": 8,
    "date": "2021-10-20 11:25:50 ",
    "price": "49325",
    "delevery": true,
    "product": [
      {
        "id": 10,
        "name": "Mills",
        "quantity": 88
      },
      {
        "id": 8,
        "name": "Gonzalez",
        "quantity": 69
      },
      {
        "id": 3,
        "name": "Battle",
        "quantity": 5
      }
    ]
  },
  {
    "_id": 10,
    "date": "2019-05-18 11:58:46 ",
    "price": "56175",
    "delevery": true,
    "product": [
      {
        "id": 4,
        "name": "Hood",
        "quantity": 62
      }
    ]
  },
  {
    "_id": 5,
    "date": "2014-01-05 02:25:41 ",
    "price": "53601",
    "delevery": true,
    "product": [
      {
        "id": 5,
        "name": "Brown",
        "quantity": 67
      }
    ]
  },
  {
    "_id": 9,
    "date": "2018-05-02 04:36:43 ",
    "price": "63558",
    "delevery": true,
    "product": [
      {
        "id": 7,
        "name": "Hatfield",
        "quantity": 20
      },
      {
        "id": 7,
        "name": "Mcgee",
        "quantity": 99
      },
      {
        "id": 9,
        "name": "Hancock",
        "quantity": 9
      }
    ]
  },
  {
    "_id": 3,
    "date": "2020-07-26 08:48:24 ",
    "price": "30877",
    "delevery": false,
    "product": [
      {
        "id": 6,
        "name": "Wright",
        "quantity": 49
      }
    ]
  },
  {
    "_id": 8,
    "date": "2014-05-24 06:35:53 ",
    "price": "1642",
    "delevery": true,
    "product": [
      {
        "id": 7,
        "name": "Nunez",
        "quantity": 95
      },
      {
        "id": 9,
        "name": "Downs",
        "quantity": 50
      },
      {
        "id": 2,
        "name": "Durham",
        "quantity": 28
      }
    ]
  },
  {
    "_id": 8,
    "date": "2015-09-25 12:28:50 ",
    "price": "49325",
    "delevery": true,
    "product": [
      {
        "id": 10,
        "name": "Mills",
        "quantity": 88
      },
      {
        "id": 8,
        "name": "Gonzalez",
        "quantity": 69
      },
      {
        "id": 3,
        "name": "Battle",
        "quantity": 5
      }
    ]
  },
  {
    "_id": 10,
    "date": "2019-05-31 02:58:46 ",
    "price": "56175",
    "delevery": true,
    "product": [
      {
        "id": 4,
        "name": "Hood",
        "quantity": 62
      }
    ]
  },
  {
    "_id": 5,
    "date": "2014-01-05 02:25:41 ",
    "price": "53601",
    "delevery": true,
    "product": [
      {
        "id": 5,
        "name": "Brown",
        "quantity": 67
      }
    ]
  },
  {
    "_id": 9,
    "date": "2018-05-02 04:36:43 ",
    "price": "63558",
    "delevery": true,
    "product": [
      {
        "id": 7,
        "name": "Hatfield",
        "quantity": 20
      },
      {
        "id": 7,
        "name": "Mcgee",
        "quantity": 99
      },
      {
        "id": 9,
        "name": "Hancock",
        "quantity": 9
      }
    ]
  },
  {
    "_id": 3,
    "date": "2020-07-26 08:48:24 ",
    "price": "30877",
    "delevery": false,
    "product": [
      {
        "id": 6,
        "name": "Wright",
        "quantity": 49
      }
    ]
  },
  {
    "_id": 8,
    "date": "2014-05-24 06:35:53 ",
    "price": "1642",
    "delevery": true,
    "product": [
      {
        "id": 7,
        "name": "Nunez",
        "quantity": 95
      },
      {
        "id": 9,
        "name": "Downs",
        "quantity": 50
      },
      {
        "id": 2,
        "name": "Durham",
        "quantity": 28
      }
    ]
  },
  {
    "_id": 8,
    "date": "2015-09-25 12:28:50 ",
    "price": "49325",
    "delevery": true,
    "product": [
      {
        "id": 10,
        "name": "Mills",
        "quantity": 88
      },
      {
        "id": 8,
        "name": "Gonzalez",
        "quantity": 69
      },
      {
        "id": 3,
        "name": "Battle",
        "quantity": 5
      }
    ]
  },
  {
    "_id": 10,
    "date": "2019-05-31 02:58:46 ",
    "price": "56175",
    "delevery": true,
    "product": [
      {
        "id": 4,
        "name": "Hood",
        "quantity": 62
      }
    ]
  },
  {
    "_id": 5,
    "date": "2014-01-05 02:25:41 ",
    "price": "53601",
    "delevery": true,
    "product": [
      {
        "id": 5,
        "name": "Brown",
        "quantity": 67
      }
    ]
  },
  {
    "_id": 9,
    "date": "2018-05-02 04:36:43 ",
    "price": "63558",
    "delevery": true,
    "product": [
      {
        "id": 7,
        "name": "Hatfield",
        "quantity": 20
      },
      {
        "id": 7,
        "name": "Mcgee",
        "quantity": 99
      },
      {
        "id": 9,
        "name": "Hancock",
        "quantity": 9
      }
    ]
  },
  {
    "_id": 3,
    "date": "2020-07-26 08:48:24 ",
    "price": "30877",
    "delevery": false,
    "product": [
      {
        "id": 6,
        "name": "Wright",
        "quantity": 49
      }
    ]
  },
  {
    "_id": 8,
    "date": "2014-05-24 06:35:53 ",
    "price": "1642",
    "delevery": true,
    "product": [
      {
        "id": 7,
        "name": "Nunez",
        "quantity": 95
      },
      {
        "id": 9,
        "name": "Downs",
        "quantity": 50
      },
      {
        "id": 2,
        "name": "Durham",
        "quantity": 28
      }
    ]
  },
  {
    "_id": 8,
    "date": "2021-10-07 04:36:43 ",
    "price": "49325",
    "delevery": true,
    "product": [
      {
        "id": 10,
        "name": "Mills",
        "quantity": 88
      },
      {
        "id": 8,
        "name": "Gonzalez",
        "quantity": 69
      },
      {
        "id": 3,
        "name": "Battle",
        "quantity": 5
      }
    ]
  },
  {
    "_id": 10,
    "date": "2021-10-08 04:36:43 ",
    "price": "56175",
    "delevery": true,
    "product": [
      {
        "id": 4,
        "name": "Hood",
        "quantity": 62
      }
    ]
  },
  {
    "_id": 5,
    "date": "2021-10-09 04:36:43 ",
    "price": "53601",
    "delevery": true,
    "product": [
      {
        "id": 5,
        "name": "Brown",
        "quantity": 67
      }
    ]
  },
  {
    "_id": 9,
    "date": "2021-10-10 17:36:43 ",
    "price": "63558",
    "delevery": true,
    "product": [
      {
        "id": 7,
        "name": "Hatfield",
        "quantity": 20
      },
      {
        "id": 7,
        "name": "Mcgee",
        "quantity": 99
      },
      {
        "id": 9,
        "name": "Hancock",
        "quantity": 9
      }
    ]
  }


]
