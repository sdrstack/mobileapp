export const slides = [

    {
      title: 'Pizza',
      description: 'Pizza',
      image: 'assets/imgs/catego_pizza.png',
      show:true
    },
    {
      title: 'DESSERTS',
      description: 'Desserts',
      image: 'assets/imgs/categ_dessert.png',
      show:true
    },
    {
      title: 'PATISSERIE',
      description: 'Pâtisserie',
      image: 'assets/imgs/categ_gateau.png',
      show:true
    },
    {
      title: 'SANDWICHERIE',
      description: 'Sandwiches',
      image: 'assets/imgs/categ_friand.png',
      show:true

    },
    {
      title: 'FRITE & SAUCE',
      description: 'Frite & Sauce',
      image: 'assets/imgs/frite.png',
      show:true

    },
    {
      title: 'SNACKS',
      description: 'Snacks',
      image: 'assets/imgs/boisson.webp',
      show:true

    },
    {
      title: 'RESTAURATION',
      description: 'Restauration',
      image: 'assets/imgs/categ_plat.png',
      show:true

    }
  ];