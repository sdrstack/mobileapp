import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../../../app/models/models';
import { environment } from '../../../environments/environment';

import 'rxjs/add/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiService } from './api.service';
import { IProductService } from '../interface/Product.interface';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json;'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ProductService extends ApiService implements IProductService {

  constructor(public http: HttpClient) {
    super(http, environment.api.basePath.toString());
  }


  getAll(): Observable<Product[]> {
    return this.get(environment.api.menuEndpoint, httpOptions);
  }
}
