import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { User } from 'src/app/models/models';
import { ApiService } from 'src/app/provider/api/api.service';
import { environment } from 'src/environments/environment';
import { IUserService } from '../interface/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService extends ApiService implements IUserService {

  constructor(public http: HttpClient) { super(http, environment.api.basePath.toString()); }
  getUser() { let u: User; return of(u); }

  Update(data: User) { }
}
