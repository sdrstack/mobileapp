import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Order } from 'src/app/models/models';
import { ApiService } from 'src/app/provider/api/api.service';
import { environment } from 'src/environments/environment';
import { IOrdersService } from '../interface/order.interface';

@Injectable({
  providedIn: 'root'
})
export class OrdersService extends ApiService implements IOrdersService {
  r: Order[];
  constructor(public http: HttpClient) { super(http, environment.api.basePath.toString()); }

  getOrders() {
    return of(this.r);
  }

  saveToCard(P: any) {

  }

  isdeliverable() {
    return true;
  }
  addQuantity(P: any, i: number) { }
  removeQantity(P: any, i: number) { }
  sendOrder(order: any) { let o: Order; return of(o);}
  getcurrentOrder(): Observable<Order> { return of(this.r[0]) }
  removeOrderfromcard(i: number) { }
  removecurrentOrder() { let o: Order; return o;};
  removeAllOrderfromcard() { };
  saveOrder() { };

}
