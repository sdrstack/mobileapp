import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Categorie } from '../../models/models';
import { environment } from '../../../environments/environment';

import 'rxjs/add/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiService } from './api.service';
import { ICategorieService } from '../interface/categorie.interface';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json;'
  })
};

@Injectable({
  providedIn: 'root'
})
export class CategorieService extends ApiService implements ICategorieService {

  constructor(public http: HttpClient) {
    super(http, environment.api.basePath.toString());
  }


  getAll(): Observable<Categorie[]> {
    return this.get(environment.api.menuEndpoint, httpOptions);
  }
}
