import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Place } from 'src/app/models/models';
import { ApiService } from 'src/app/provider/api/api.service';
import { IPlaceService } from '../interface/place.interface';

@Injectable({
  providedIn: 'root'
})
export class PlacesService extends ApiService implements IPlaceService {

  constructor() { }

  getAll(): Observable<Place[]>{
    let Places : Place[] = [];
    return of(Places);
  };
}
