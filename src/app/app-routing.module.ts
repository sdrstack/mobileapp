import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { IntroGuard } from './guards/intro.guard';

const routes: Routes = [
  {
    path: 'tabs',
    loadChildren: () => import('./pages/tabnav/tabnav.module').then(m => m.TabnavPageModule),
    canLoad: [IntroGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule),
  },
  {
    path: 'intro',
    loadChildren: () => import('./pages/sliders/sliders.module').then(m => m.SlidersPageModule)
  },
  {
    path: '',
    redirectTo: 'intro',
    pathMatch: 'full'
  },
  {
    path: 'orderconfirm',
    loadChildren: () => import('./pages/orders/orderconfirm/orderconfirm.module').then(m => m.OrderconfirmPageModule),
    canLoad: [IntroGuard]
  },
  {
    path: 'card',
    loadChildren: () => import('./pages/orders/card/card.module').then(m => m.CardPageModule),
    canLoad: [IntroGuard]
  },
  {
    path: 'history',
    loadChildren: () => import('./pages/history/history.module').then(m => m.HistoryPageModule)
  },
  {
    path: 'product',
    loadChildren: () => import('./pages/product/product.module').then(m => m.ProductPageModule)
  },
  {
    path: '**',
    loadChildren: () => import('./pages/error/error.module').then(m => m.ErrorPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
