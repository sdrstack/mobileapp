import { StorageserviceService } from './provider/storageservice.service';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { IonicStorageModule } from '@ionic/storage-angular';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { TabnavPageModule } from './pages/tabnav/tabnav.module';
import { SharedModule } from './component/shared/shared.module';
import { environment } from 'src/environments/environment';
import { FilterPipe } from './provider/pipe/filter.pipe';
import { OrdersService } from './service/mock/orders.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {AutocompleteLibModule} from 'angular-ng-autocomplete';
// import { CategorieService } from './service/mock/categorie.service';
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  exports: [AppComponent],

  imports: [
    AutocompleteLibModule,
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    TabnavPageModule,
    HttpClientModule,
    SharedModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot({
      name: '__sdrapp'  /// setting local database name
    }),
    AppRoutingModule],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, StorageserviceService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
