import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';
export const INTRO_KEY = 'intro-seen';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Injectable({
  providedIn: 'root'
})
export class IntroGuard implements CanLoad {
  constructor(private router: Router) { }

  async canLoad(): Promise<boolean> {
    const hasSeenIntro = Storage.get({ key: INTRO_KEY });
    if (hasSeenIntro && ((await hasSeenIntro).value === "true")) {
      return true;
    } else {
      this.router.navigateByUrl('/intro', { replaceUrl: true });
    }
  }
}
