import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { UserCartPage } from './user-cart';

@NgModule({
  declarations: [
    UserCartPage,
  ],
  imports: [
    IonicModule
  ],
})
export class UserCartPageModule { }
