import { Component } from "@angular/core";
import { NavController, NavParams } from "@ionic/angular";

//@IonicModule()
@Component({
  selector: "page-user-cart",
  templateUrl: "user-cart.html"
})
export class UserCartPage {
  address;
  constructor(public navCtrl: NavController, public navParams: NavParams) { }

  ionViewDidLoad() {
    console.log("ionViewDidLoad UserCartPage");
  }
  checkout() {
    this.navCtrl.navigateForward("checjoutPage");

  }
  ionViewWillEnter() {
    this.address = localStorage.getItem("deliveryAddress");
  }
  changeAddress() {
    this.navCtrl.navigateForward("NewAddressPage");
  }
}
