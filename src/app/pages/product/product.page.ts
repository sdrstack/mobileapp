import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {

  product: any = {
    image: environment.image.url,
    restaurant: "Sandwich du Roi",
    livrable: true,
    titre: "Pizza au poulet",
    description: environment.message.NO_DATA,
    ingredients: [],
    prix: 2000,
  };
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

}
