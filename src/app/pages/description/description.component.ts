import { Component, OnInit } from '@angular/core';
import {
  NavController,
  // NavParams, 
  ModalController,
} from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';



import { DatapasseService } from '../../provider/datapasse/datapasse.service';


@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss'],
})
export class DescriptionComponent implements OnInit {

  subscription: Subscription;

  description: any;

  constructor(
    public navCtrl: NavController,
    //  public navParams: NavParams,
    private router: Router,
    private modalCtrl: ModalController,
    private dataPasse: DatapasseService
  ) { }

  ngOnInit() { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DescriptionPage');
    this.subscription = this.dataPasse.currentDescrip.subscribe(item => {
      this.description = item;
    });
  }

  goOrders() {
    this.modalCtrl.dismiss();
    // this.navCtrl.push('OrderHistoryPage');
    this.router.navigate(['/order-history']);

  }
  Shopping() {
    this.modalCtrl.dismiss();
  }

}
