import { Component, OnInit } from '@angular/core';
import {
  NavController,
  // NavParams,
  ModalController,
} from '@ionic/angular';
import { Subscription } from 'rxjs';
import { DatapasseService } from '../../provider/datapasse/datapasse.service';


@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.scss'],
})
export class IngredientComponent implements OnInit {

  subscription: Subscription;
  ingredients: any;
  checkboxModel = [];
  isIndeterminate: boolean;
  masterCheck = true;



  checkallModel = [{
    value: false,
    text: 'tout'
  }];

  selOptions: any;
  id: any;

  constructor(
    public navCtrl: NavController,
    // public navParams: NavParams,
    private modalCtrl: ModalController,
    private dataPasse: DatapasseService,
  ) { }

  ngOnInit() {
    this.checkMaster();
    this.subscription = this.dataPasse.currentIngredient.subscribe(item => {
      this.ingredients = item.ingredients;
      this.id = item.id;
      // console.log(item)

      this.ingredients.forEach(element => {
        this.checkboxModel = this.checkboxModel.concat({
          value: false,
          text: element.ingredient
        });

      });
    });
  }

  checkboxChange() {
    const selOption = this.checkboxModel.filter(f => f.value)
      .map(c => ({ ingredient: c.text }));
    this.selOptions = {
      idRepas: this.id,
      selOption
    };

    const totalItems = this.checkboxModel.length;
    const checked = this.checkallModel.filter(f => f.value).length;
    this.masterCheck = (checked === totalItems);
    this.isIndeterminate = (checked > 0 && checked < totalItems);
  }

  checkMaster() {
    setTimeout(() => {
      let selOption = [];
      this.checkboxModel.forEach(obj => {
        obj.value = this.masterCheck;
      });


      const checkboxes = this.checkboxModel;


      for (let i = 0; i < checkboxes.length; ++i) {
        if (checkboxes[i].value) {
          selOption = selOption.concat({
            ingredient: checkboxes[i].text

          });
          // selOption.push(checkboxes[i].text);
        }

      }
      this.selOptions = selOption;

    });



  }

  closeModal() {
    this.dataPasse.passChoiceIngredient(this.selOptions);
    this.modalCtrl.dismiss();
  }

}
