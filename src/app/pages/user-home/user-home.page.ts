import { Component,OnInit} from '@angular/core';
import { environment } from './../../../environments/environment';
import {
  NavController,
  ToastController,
  ModalController,
  Platform, LoadingController, AlertController,
} from '@ionic/angular';
import {  of, Subscription } from 'rxjs';
import { Router} from '@angular/router';
import { slides as data } from '../../service/data/filter-slides';
import { SliderItem } from 'src/app/models/models';
// import { ICategorieService } from 'src/app/service/interface/categorie.service';
import { MockCategorieService } from 'src/app/service/mock/categorie.service';
import { CategorieService as MenuService } from 'src/app/service/api/categorie.service';


@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.page.html',
  styleUrls: ['./user-home.page.scss'],
  providers: [
    {
      provide: MenuService,
      useClass: environment.providers.MENU,
  }]
})

export class UserHomePage implements OnInit {
  titre = environment.titre;
  show: Boolean=false;
  status: Boolean=false;
  listMenu = [];
  filterListMenu = [];
  subscription: Subscription;
  panier = [];
  searchs =  [];
  nb:number=1;
  // private _search : BehaviorSubject<string[]> = new BehaviorSubject<string[]>(this.searchs);
  // public searchList$ : Observable<string[]> = this._search.asObservable();
  filtre = false;
  search : string;


  slides  : SliderItem[];
  slideOpts = {
    initialSlide: 0,
    speed: 2000,
    slidesPerView: 4,
    dots:false,
    point: false,
    autoplay: false
  };

  constructor(
    public navCtrl: NavController,
    private alertController: AlertController,
    private loadingCtrl: LoadingController,


    private varMenu: MenuService,


    public toastController: ToastController,
    public platform: Platform,
    public router: Router,
    public modalController: ModalController,
  ) {


  }

  ngOnInit() {
    of(data).subscribe(res =>{this.slides= res});
    this.history();
    this.menulist();

  }





  history() {
    if (sessionStorage.getItem(environment.values.USER_CART)) {
      this.panier = JSON.parse(sessionStorage.getItem(environment.values.USER_CART));
    }
  }


  loader() {
    return this.loadingCtrl.create({
      message: environment.message.MENU_LOADING
    });
  }
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Oups!',
      message: environment.message.SERVICE_UNAVAILABLE,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.exitApp();
          }
        }
      ]
    });

    await alert.present();
  }

  exitApp() {
    navigator['app'].exitApp();
  }

  async menulist() {
    const loading = await this.loader();
    loading.present();
    this.varMenu.getAll().subscribe(
     (res) => {
        if (this._notNull(res)) {
          this.filterListMenu=this.listMenu = res;
        }
        loading.dismiss();

      },
      (error) => {
        console.error(error);
        loading.dismiss();
        this.presentAlertConfirm();
      }
    );
  }

  _notNull(data) {
    return (
      data.length > 0 &&
      data.map((e) => {
        return e.cat !== '';
      })
    );
  }



  async handleButtonClick() {
    const loading = await this.loadingCtrl.create({
      message: 'Chargement du menu...',
      duration: 4
    });

    await loading.present();
  }


  processCart() {
    this.router.navigate(['user-cart']);
  }

  getItemTitle(itemTitle : string) {

    (this.searchs.indexOf(itemTitle.toLowerCase()) > -1) ?   this.searchs=this.searchs.filter(e=> e !==itemTitle.toLocaleLowerCase()) : this.searchs.push(itemTitle.toLowerCase()) ;
    this.filterListMenu=(this.searchs.length==0) ? this.filterListMenu : this.listMenu.filter(({ cat }) => this.searchs.includes(cat.toLowerCase()));

    for(let i=0;i<this.filterListMenu.length;i++){
      if(this.filterListMenu[i].repas.length==0){
        this.show=true;
      }else{
        this.show=false;
        break;
      }
    }

  }




  selectEvent(){
    this.status=true
  }


  selectEventBlur(){
    //gitconsole.log(this.search.length==0)
    try {
      this.status=!(this.search.length==0)
    } catch (error) {
      this.status=false

    }


  }

  onChange(){
    this.status=!(this.search.length==0)

  }



}
