import { ProductComponent } from './../../component/product/product.component';
import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { UserHomePageRoutingModule } from './user-home-routing.module';
import { UserHomePage } from './user-home.page';
import { SharedModule } from 'src/app/component/shared/shared.module';
import { FilterPipe,FilterCatPipe } from 'src/app/provider/pipe/filter.pipe';
// import { CategorieService } from 'src/app/service/mock/categorie.service';




@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserHomePageRoutingModule,
    SharedModule,
  ],
  exports: [],
  providers: [],
  declarations: [UserHomePage, ProductComponent,FilterPipe,FilterCatPipe],

})
export class UserHomePageModule { }
