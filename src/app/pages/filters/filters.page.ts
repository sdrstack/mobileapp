import { Component, OnInit } from '@angular/core';
import {
  NavController
  // NavParams
} from '@ionic/angular';
import { Subscription } from 'rxjs';
import { DatapasseService } from '../../provider/datapasse/datapasse.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.page.html',
  styleUrls: ['./filters.page.scss'],
})
export class FiltersPage implements OnInit {

  subscription: Subscription;

  structure: any = { lower: 5, upper: 20 };
  checkboxModel: Array<any>;
  isIndeterminate: boolean;
  masterCheck: boolean;
  loading: any;


  selOptions: Array<string>;

  constructor(
    public navCtrl: NavController,
    // public navParams: NavParams,
    private dataPasse: DatapasseService
  ) {
    // this.checkboxModel = this.navParams.get('categories').map(e => {
    //   return { value: e.state, text: e.cat };
    // });
    this.masterCheck = this._isMasterEnabled();
    this.checkboxChange();
  }

  ngOnInit() {
  }

  _isMasterEnabled() {
    this.masterCheck = true;
    this.checkboxModel.forEach(e => { this.masterCheck = this.masterCheck && (e.value); });
    return this.masterCheck;
  }


  checkboxChange() {
    this.selOptions = this.checkboxModel.filter(e => e.value === true).map(item => item.text); // selected Options
    const totalItems = this.checkboxModel.length;
    const checked = this.selOptions.length;  // Number of checkbox checked ...
    this.masterCheck = (checked === totalItems); // masterckecked is true when all item selected
    this.isIndeterminate = (checked > 0 && checked < totalItems); // indterminate status when nb check
    //    sessionStorage.setItem('checkbox', JSON.stringify(this.checkboxModel));
  }

  checkMaster() {
    this.checkboxModel.forEach(obj => { obj.value = this.masterCheck; });
    this.checkboxChange();
  }


  filter(el) {
    this.dataPasse.passCat(this.selOptions);
    this.navCtrl.pop();
  }
  clear() {
    // code for clear filter
    this.navCtrl.pop();
  }

}
