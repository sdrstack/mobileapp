import { Component, OnInit } from '@angular/core';
import {
  NavController
  // NavParams
} from '@ionic/angular';
import { FieldValidatorService } from './../../provider/field-validator/field-validator.service';
import { RouterModule } from '@angular/router';
import { environment } from 'src/environments/environment';
import { StorageserviceService } from './../../provider/storageservice.service';
import { Plugins } from '@capacitor/core';
import { INTRO_KEY } from 'src/app/guards/intro.guard';
import {User} from 'src/app/models/models'
const { Storage } = Plugins;

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  private user: User = {
    name: '',
    firstname: '',
    telephone: '',
    address: '',
  }
  numberUser: any;
  savemessage: string;
  isEnabled: boolean;
  disable: boolean;
  number: any;
  warning = '';
  message = '';


  constructor(
    public navCtrl: NavController,
    public router: RouterModule,
    public fieldValidatorProvider: FieldValidatorService,
  ) { }

  ngOnInit() {
  }

  ionViewDidLoad() { }

  validateNumbers() {
    const a = this.fieldValidatorProvider.validateNumber(this.user.telephone);
    this.warning = a.info;
    this.isEnabled = a.state;
  }

  checkIsEnableds() {
    return this.fieldValidatorProvider.checkIsEnabled(this.isEnabled);
  }

  saveNumber() {
      Storage.set({
        key: INTRO_KEY,
        value: 'true'
      });

    Storage.set({
      key: 'USER_IDENTITY',
      value: JSON.stringify(this.user)
    });

    this.navCtrl.navigateRoot('/home');
  }
}
