import { Component, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-error',
  templateUrl: './error.page.html',
  styleUrls: ['./error.page.scss'],
})
export class ErrorPage implements OnInit {

  constructor(
     public navCtrl: NavController,
     public router: RouterModule,) {}

  ngOnInit() {
  }

  returnHome(){
    this.navCtrl.navigateForward('/home');
  }

}
