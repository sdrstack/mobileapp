import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntroGuard } from 'src/app/guards/intro.guard';
import { TabnavPage } from './tabnav.page';

const routes: Routes = [
  {
    path: '',
    component: TabnavPage,
    children: [

      {
        path: 'home',
        loadChildren: () => import('../user-home/user-home.module').then(m => m.UserHomePageModule),
        canLoad: [IntroGuard]
      },
      {
        path: 'cart',
        loadChildren: () => import('../user-cart/user-cart.module').then(m => m.UserCartPageModule)
      },
      {
        path: 'profil',
        loadChildren: () => import('../user-profile/user-profile.module').then(m => m.UserProfilePageModule)
      },
      {
        path: 'history',
        loadChildren: () => import('../history/history.module').then(m => m.HistoryPageModule)
      },

      {
        path: 'product',
        loadChildren: () => import('../product/product.module').then(m => m.ProductPageModule),
        canLoad: [IntroGuard]
      },
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/intro',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabnavPageRoutingModule { }
