import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-sliders',
  templateUrl: './sliders.page.html',
  styleUrls: ['./sliders.page.scss'],
})

export class SlidersPage implements OnInit {

  slides = [
    {
      title: 'Commandez votre repas',
      description: 'Choisissez et commandez  votre repas en 30 secondes',
      image: 'assets/imgs/test/1_welcomescreen.png',
    },
    {
      title: 'Selectionnez votre menu',
      description: 'Obtenez tout ce que vous voulez sur une large gamme de menu disponible',
      image: 'assets/imgs/test/3_welcomescreen.png'
    },
    {
      title: 'Mangez au bon moment',
      description: 'Recevez votre commande en moins de 30 minutes  ou choisir votre heure de livraison',
      image: 'assets/imgs/test/2_welcomescreen.png',
    }
  ];
  slideOpts = {
    initialSlide: 0,
    speed: 2000,
    slidesPerView: 1,
    autoplay: true
  };


  constructor(
    private router: Router,
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
    console.log('init');
  }

  start() {
    this.navCtrl.navigateRoot('/login');
  }
}
