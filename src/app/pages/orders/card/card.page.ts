import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrdersService } from 'src/app/service/mock/orders.service';
import { NavController } from '@ionic/angular';
import { Order } from 'src/app/models/models';

@Component({
  selector: 'app-card',
  templateUrl: './card.page.html',
  styleUrls: ['./card.page.scss'],
})
export class CardPage implements OnInit {
  i: number = 0;
  show: boolean = true;
  currentOrder: Order;
  constructor(
    private router: Router,
    private Orders: OrdersService,
    private navCtrl:NavController,

  ) { }

  Somme(){
    this.currentOrder.totalAmount = this.currentOrder.item.map(o=>{ return o.item.prix * o.quantity}).reduce((e,a) => e+a , this.currentOrder.deliveryFees);
    return this.currentOrder.totalAmount;
  }



  ngOnInit() {
    this.Orders.getcurrentOrder().subscribe(order => this.currentOrder = order);
  }
  ionViewWillEnter(){
    this.Orders.getcurrentOrder().subscribe(order => this.currentOrder = order);
    this.Orders.currentInfosProduct$;
  }

  Up(i) {
    this.Orders.addQuantity(this.currentOrder,i)
  }

  Down(i) {

    this.Orders.removeQantity(this.currentOrder,i)
  }

  deletFromCard(i) {
    this.Orders.removeOrderfromcard(i)
  }

  ClearAll(){
    this.Orders.removeAllOrderfromcard()

  }


  confirmOrder() {
    this.router.navigateByUrl('/orderconfirm');
  }
  back(){
    //this.navCtrl.pop();
    this.router.navigateByUrl('/home');
  }

  Commande(){
    this.router.navigateByUrl('/orderconfirm');
  }

}
