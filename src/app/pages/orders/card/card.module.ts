import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CardPageRoutingModule } from './card-routing.module';

import { CardPage } from './card.page';
import { SharedModule } from 'src/app/component/shared/shared.module';
import {  TwoDigitPipe } from 'src/app/provider/pipe/filter.pipe';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CardPageRoutingModule,
    SharedModule,
  ],

  declarations: [CardPage, TwoDigitPipe],
})
export class CardPageModule {}
