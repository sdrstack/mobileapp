import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrdersComponent } from './orders.component';
import { IntroGuard } from 'src/app/guards/intro.guard';


const routes: Routes = [
  {
    path: '',
    component: OrdersComponent,
    children: [
      {
        path: 'orderconfirm',
        loadChildren: () => import('./orderconfirm/orderconfirm.module').then(m => m.OrderconfirmPageModule),
        canLoad: [IntroGuard]
      },
      {
        path: 'card',
        loadChildren: () => import('./card/card.module').then(m => m.CardPageModule),
        canLoad: [IntroGuard]
      },
      {
        path: '',
        redirectTo: '/card',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/card',
    pathMatch: 'full'
  }
]
  ;

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
