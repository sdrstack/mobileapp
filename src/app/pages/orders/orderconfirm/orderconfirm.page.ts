import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController, AlertController, LoadingController } from '@ionic/angular';
import { Order } from 'src/app/models/models';
import { PlaceService } from 'src/app/service/mock/places.service';
import { PopoverOrderconfirmComponent } from '../../../component/popover-orderconfirm/popover-orderconfirm.component';
import { OrdersService } from '../../../service/mock/orders.service'
import { environment } from '../../../../environments/environment';
import { ToastController } from '@ionic/angular';




@Component({
  selector: 'app-orderconfirm',
  templateUrl: './orderconfirm.page.html',
  styleUrls: ['./orderconfirm.page.scss'],
})
export class OrderconfirmPage implements OnInit {
  show: boolean = false;
  areaList: any = null;
  keyword = 'name';
  search: string = '';
  selectedArea: any;
  heure:number;
  disablePin:boolean=false;

  currentOrder: Order;

  constructor(public alertController: AlertController,
    public popoverController: PopoverController,
    public placeService: PlaceService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private orderService: OrdersService,
    public toastCtrl: ToastController,

  ) { }

  ngOnInit() {
    this.placeService.getAll().subscribe(
      (res) => {
        this.areaList = res
      },
      (error) => {
        console.error(error);
      }
    );

    this.orderService.getcurrentOrder().subscribe(data => { this.currentOrder = data; })

  }

  async presentPopover(value: string) {
    const popover = await this.popoverController.create({
      component: PopoverOrderconfirmComponent,
      componentProps: { message:  value  },
      cssClass: 'my-custom-class',
      //  event: ev,
      animated: true,
      backdropDismiss: false,
      showBackdrop: true,
      translucent: true
    },
    );

    await popover.present();

    const { role } = await popover.onDidDismiss();

    if (role) {
      this.router.navigate['/history'];
    }

  }

  selectEvent(event: any) {
    this.selectedArea = event.name;
    this.currentOrder.deliveryFees = event.price;
    this.search = event.locations.find(loc => loc.includes(this.search));

  }

  getsearch(event: string) {
    
    this.search = event;
  }

  setItem(item: string) {

    this.search = item;
   
  }


  myCustomFilter(items: any[], query: string): any[] {
   
    return items.map(e => {
      return (query.length > 0) ? {
        
        name: e.name,

        locations: e.locations.filter(area => area.toLowerCase().indexOf(query.toLowerCase()) !== -1),
        price: e.price,
      } : e
    }).filter(el => el.locations.length > 0);
  }

  getback() {
    this.router.navigate(['/card'])
  }

  loader() {
    return this.loadingCtrl.create({
      message: 'Commande en cours'
    });
  }

  async ConfirmOrder() {
    const loading = await this.loader();
    loading.present();
    this.orderService.sendOrder(this.currentOrder).subscribe((response) => {

      loading.dismiss();
      this.presentPopover(response.number.toString());


    })
    this.orderService.saveOrder();
    this.currentOrder=this.orderService.removecurrentOrder();
     

  }

  async presentToast(msg : string) {
    const toast = await this.toastCtrl.create({
      message:msg,
      duration: environment.notification.duration
    });
    toast.present();
  }

  toggleClick(event){
    let date = this.currentOrder.dateDelivery.split('T')[1];
    this.heure = +date.split(':')[0];

    event.target.checked = (this.heure < 19) && (this.orderService.isdeliverable());

    if(this.heure >=19){
      this.presentToast("Désolé nous n'avons pas de livreur disponible pour l'heure sélectionnée.")
    }
    if (!this.orderService.isdeliverable()) {
      this.presentToast("Désolé votre panier contient des produits non livrables");
    }


  }



}


