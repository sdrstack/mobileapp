import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule, NavParams } from '@ionic/angular';
import { UserProfilPageRoutingModule } from './user-profile-routing.module';
import { UserProfilePage } from './user-profile.page';
import { SharedModule } from 'src/app/component/shared/shared.module';
@NgModule({
  declarations: [
    UserProfilePage,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    UserProfilPageRoutingModule,
    SharedModule
  ],
  providers: [NavParams]
})
export class UserProfilePageModule { }
