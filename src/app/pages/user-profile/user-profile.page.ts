import { Component, OnInit } from "@angular/core";
import {
  NavController,
  NavParams,
  ToastController,
} from "@ionic/angular";
import { SharedDataService } from "src/app/provider/gloobal-foo/global-foo.service"
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { FieldValidatorService } from './../../provider/field-validator/field-validator.service';
import { environment } from 'src/environments/environment';
import { StorageserviceService } from "src/app/provider/storageservice.service";
import { RouterModule } from "@angular/router";
import { BehaviorSubject, Observable } from "rxjs";
import { UserService } from "../../service/mock/user.service";
//import { IonRouterOutlet, Platform } from '@ionic/angular';
import { Plugins } from '@capacitor/core';
const { App } = Plugins;


/**
 * Generated class for the UserProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-user-profile",
  templateUrl: "user-profile.html",
  styleUrls: ["./user-profile.scss"]
})
export class UserProfilePage implements OnInit {
  isEnabled: boolean;
  readonly: boolean = true;
  disable: boolean;
  warning = '';
  status: boolean = false;
  validations_form = new FormGroup(
    {
      telephone: new FormControl('', Validators.compose([
        Validators.maxLength(8),
        Validators.required,
        Validators.pattern('^[0-9]'),

      ])),
      name: new FormControl('', Validators.required),
      firstname: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
    }
  );

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public router: RouterModule,
    public fieldValidatorProvider: FieldValidatorService,
    public toastController: ToastController,
    public formBuilder: FormBuilder,
    public User: UserService,
    //private localStorage: StorageserviceService

  ) {
    
  }
  ngOnInit() {

    this.User.getUser().subscribe(infos => {

      this.validations_form.controls["name"].setValue(infos.name);
      this.validations_form.controls["firstname"].setValue(infos.firstname);
      this.validations_form.controls["telephone"].setValue(infos.telephone);
      this.validations_form.controls["address"].setValue(infos.address);
    });
  }
  validation_messages = {
    'name': [
      { type: 'required', message: 'Name is required.' }
    ],
    'firstname': [
      { type: 'required', message: 'Firstname is required.' }
    ],
    'address': [
      { type: 'required', message: 'Address is required.' }
    ],
  }

  modify() {
    this.readonly = !this.readonly;
    this.status = true;
  }
  async update() {

    this.readonly = true;
    this.status = false;
    this.User.Update(this.validations_form.value);
    this.notify();
  }

  async notify() {
    const toast = await this.toastController.create({
      message: 'Modification réussie',
      duration: 2000
    });
    toast.present();

  }

  validateNumbers() {

    const a = this.fieldValidatorProvider.validateNumber(this.validations_form.controls["telephone"].value);
    this.warning = a.info;
    this.isEnabled = a.state;
  }
  doubleClickFunction() {
    this.readonly = !this.readonly;
    this.status = true;
  }
  tap() {
    this.readonly = !this.readonly;
    this.status = true;
  }
  quit() {
    this.navCtrl.navigateRoot('/home');
  }

  deconexion(){
    //this.foundation.logout();
    // does navigate user
    //this.localStorage.init();
   // navigator['app'].exitApp()
    App.exitApp()
    //this.navCtrl.navigateRoot('/login');
  }










}
