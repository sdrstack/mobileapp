import { Component, OnInit,ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { OrdersService } from '../../service/mock/orders.service'

export const STEP = 8;


@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  listOrder: any[];
  slice: number = 5;
  constructor(
    private Orders: OrdersService,
  ) { }


  ngOnInit() {

    this.Orders.getOrders().subscribe((e) => {
      this.listOrder = e.sort(function (a, b) {
        return new Date(b.date).getTime() - new Date(a.date).getTime();
      });
    })

  }

  loadData(event) {
    setTimeout(() => {
      this.slice += 8;
      event.target.complete();
      if (this.listOrder.length == 2) {
        event.target.disabled = true;
      }
    }, 500);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }
}
