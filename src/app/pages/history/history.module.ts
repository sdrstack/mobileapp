import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HistoryPageRoutingModule } from './history-routing.module';

import { HistoryPage } from './history.page';
import { SharedModule } from 'src/app/component/shared/shared.module';
import { TimeAgo} from 'src/app/provider/pipe/filter.pipe';
import { HistoryItemComponent } from 'src/app/component/history-item/history-item.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HistoryPageRoutingModule,
    SharedModule
  ],
  declarations: [HistoryPage, HistoryItemComponent,TimeAgo]
})
export class HistoryPageModule { }
